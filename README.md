### Hi there 👋

<!--
**eternaltyro/eternaltyro** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitLab profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

I am a DevSecOps and Site Reliability Engineer. I wrestle with infrastructure automation for my day job. I help enterprises streamline their infrastructure and application architecture, wrangle costs, and dominate issues with security, compliance, stardardisation and documentation. I also volunteer contributor for some open source applications in my free time.

- 💬 Ask me about technology ethics, labour rights, and the perils of solutionism (apart from what is above)
- 🌱 I’m currently learning:
  - Open Policy Agent (<abbr title="Open Policy Agent">OPA</abbr>) and Rego (and using <abbr title="Open Policy Agent">OPA</abbr> to enforce policies against Terraform)
  - Rust & Go - let's see how that goes.
  - Nix - the functional package manager, Nix - the related language, and NixOS - the GNU/Linux distribution based on it.

Not all me's online are me.

